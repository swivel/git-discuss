#!/usr/bin/env sh

# Later, check to see if there's a remote
# if there is, try and fetch their discussions
# and then use that as a base
# rather than always switching to an orphan and
# creating a brand new tree...
# We want everyone to share the same tree
# G_DEFAULT_REMOTE=$(git config checkout.defaultRemote)

#getTargetBranch()
{
	GIT_CUR_BRANCH=$(git branch --show-current)
	[[ $? -ne 0 ]] && {
		exit 1
	}

	[[ -z "$GIT_CUR_BRANCH" ]] && {
		echo "fatal: HEAD is not a branch"
		exit 1
	}
}

_GIT_DIR=$(git rev-parse --show-toplevel)/.git
D_DISCUSS_REFS=$_GIT_DIR/refs/discuss
F_DISCUSS_CUR_REF=$D_DISCUSS_REFS/foobar

[[ ! -f $F_DISCUSS_CUR_REF ]] && {
	F_DISCUSS_ROOT_REF=$D_DISCUSS_REFS/root
	[[ ! -f $F_DISCUSS_ROOT_REF ]] && {
		echo "fatal: git-discuss is not setup in this repository yet"
		echo "You can set it up, with"
		echo -e "\n  git discuss setup\n"
		exit 1
	}

	cat $F_DISCUSS_ROOT_REF > $F_DISCUSS_CUR_REF
	[[ $? -ne 0 ]] && {
		echo "fatal: failed"
		exit 1
	}
}

D_DISCUSSTREE=$(mktemp -p /tmp -d git-discuss-tree-XXXXXXXXXX)
DISCUSS_TREE_NAME=$(basename $D_DISCUSSTREE)

git_discuss_clean_worktree() {
	git worktree remove -f $D_DISCUSSTREE > /dev/null 2>&1
	rm -rf $D_DISCUSSTREE > /dev/null 2>&1
}
trap "git_discuss_clean_worktree" 0
git worktree add -f $D_DISCUSSTREE $(< $D_DISCUSS_REFS/$GIT_CUR_BRANCH)

#git switch --orphan $DISCUSS_TREE_NAME
#git commit --allow-empty -m 'root'
#
#mkdir -p $D_DISCUSS_REFS
#touch $D_DISCUSS_REFS/$GIT_CUR_BRANCH
#
#git rev-parse HEAD > $D_DISCUSS_REFS/root
#git rev-parse HEAD > $D_DISCUSS_REFS/$GIT_CUR_BRANCH
#
#git checkout $GIT_CUR_BRANCH
#git branch -D $DISCUSS_TREE_NAME
#
## This is how we "checkout" the current branch's discussions
#git worktree add -f $D_DISCUSSTREE $(< $D_DISCUSS_REFS/$GIT_CUR_BRANCH)

