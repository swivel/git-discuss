#!/usr/bin/env sh

# Later, check to see if there's a remote
# if there is, try and fetch their discussions
# and then use that as a base
# rather than always switching to an orphan and
# creating a brand new tree...
# We want everyone to share the same tree
# G_DEFAULT_REMOTE=$(git config checkout.defaultRemote)

[[ ! -z "$(git status --porcelain)" ]] && {
	echo "error: cannot setup git-discuss: Your index contains uncommitted changes."
	echo "error: Please commit or stash them."
	exit 1;
}

#getTargetBranch()
{
	GIT_CUR_BRANCH=$(git branch --show-current)
	[[ $? -ne 0 ]] && {
		exit 1
	}

	[[ -z "$GIT_CUR_BRANCH" ]] && {
		echo "fatal: HEAD is not a branch"
		exit 1
	}
}

_GIT_DIR=$(git rev-parse --show-toplevel)/.git


D_DISCUSSTREE=$(mktemp -p /tmp -d git-discuss-tree-XXXXXXXXXX)
DISCUSS_TREE_NAME=$(basename $D_DISCUSSTREE)

git switch --orphan $DISCUSS_TREE_NAME
git commit --allow-empty -m 'root'

D_DISCUSS_REFS=$_GIT_DIR/refs/discuss

mkdir -p $D_DISCUSS_REFS
touch $D_DISCUSS_REFS/$GIT_CUR_BRANCH

git rev-parse HEAD > $D_DISCUSS_REFS/root
git rev-parse HEAD > $D_DISCUSS_REFS/$GIT_CUR_BRANCH

git checkout $GIT_CUR_BRANCH
git branch -D $DISCUSS_TREE_NAME

# This is how we "checkout" the current branch's discussions
git worktree add -f $D_DISCUSSTREE $(< $D_DISCUSS_REFS/$GIT_CUR_BRANCH)

